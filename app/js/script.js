// Left sidebar menu
var all_sidebar_elements = $('.left-sidebar__dropdown-content').hide();

$('.left-sidebar__list').click(function(e){
    var thisElement = $(this).children().find('.left-sidebar__dropdown-content'),
        isShowing = thisElement.is(":visible");

    // Hide all elements
    all_sidebar_elements.hide();

    // If our span *wasn't* showing, show it
    if (!isShowing) {
        thisElement.show();
    }

    e.preventDefault();
});

// Main menu
if(window.innerWidth<=1200){
    var all_mainbar_elements = $('.main-menu__dropdown-content').hide();
}

$('.main-menu__list').click(function(e){
    if(window.innerWidth<=1200){
        var thisElement = $(this).children().find('.main-menu__dropdown-content'),
        isShowing = thisElement.is(":visible");

    // Hide all elements
    all_mainbar_elements.hide();

    // If our span *wasn't* showing, show it
    if (!isShowing) {
        thisElement.show();
    }

    e.preventDefault();
    }   
});

if(window.innerWidth>768 && window.innerWidth<1200){
    $('.main-menu__brand').html("Subotički Gritersi");
}